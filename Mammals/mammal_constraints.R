logoing_func<-function(logo, x, y, size){
  dims<-dim(logo)[1:2] #number of x-y pixels for the logo
  IAR<-dims[1]/dims[2] #image aspect ratio
  limits <- par("usr")
  AAR<-abs(limits[3]-limits[4])/abs(limits[1]-limits[2]) #axes aspect ratio
  PAR<-par("pin")[2]/par("pin")[1]
  polygon(c(0,0),c(0,0), col=rgb(0,0,0,alpha = 1))
  rasterImage(logo, x-(size/2), y-(IAR*AAR/PAR*size/2), x+(size/2), y+(IAR*AAR/PAR*size/2), interpolate = FALSE)
}

#Mammal Constraints####
pdf(file = "Mammal Constraints.pdf", width = 24, height = 12)
layout(matrix(c(1,2),nrow=1, byrow = TRUE))

colors = c(rgb(230/255, 159/255, 0/255), rgb(0/255, 158/255, 115/255), rgb(86/255, 180/255, 233/255))

#individual curves (phocidae BMR)
par(mar=c(5.1,5.1,3.1,3.1), yaxs="i", xaxs="i", cex = 2.25, las=1)
#1:1
#curve(1*x^1, .01, 100000, lwd = 2, lty = "dashed", col = "grey60")
#https://www.jstor.org/stable/pdf/4803.pdf
curve(7.5*x^.71, .01, 100000, lwd = 6, col = colors[1], xlab=expression(paste("Body Mass (log"[10]," g)")), ylab=expression(paste("Rate (log"[10]," W)")), log="xy", xaxt="n", yaxt="n", ylim=c(.5,25000))
#http://www.nrcresearchpress.com/doi/pdf/10.1139/z86-047
curve(.0484*39.8*(x^.87), .01, 100000, lwd = 6, col = colors[2], add = TRUE)
#http://www.nrcresearchpress.com/doi/pdf/10.1139/f88-121
#http://www.sciencedirect.com/science/article/pii/S0022519383711355
curve(11.4*.2*((x/3.03E-6)^(1/3.09))/1000*30/log(1/.71), .01, 100000, lwd = 6, col = colors[3], add = TRUE)
axis(side=2, at=c(1,10,100,1000,10000), labels = c(0,1,2,3,4))
axis(side=1, at=c(.01,.1,1,10,100,1000,10000,100000), labels = c(1:8))
text(x = 31000, y = 1,  cex=2, adj=c(.5,.5), labels=LETTERS[1])
box()
legend("topleft", cex = .85, inset = 0, bty= "n", c("Feeding (F)","Basal Metabolism (M)","Heat Loss (H)"), col = c(colors), lwd = 6, lty = 1)

#single curve (phocidae BMR)
library(rphylopic)
source("histStack.R")
uuids <- c("23389deb-fe10-4c97-87c4-a606ebd1fec3","0b5c6b41-3a44-4c9e-869a-63ed54bf7c65","64c27f60-ffbf-4ee8-ae1e-726042c477ff","a67f24a9-6d75-420c-b437-6817da5cec1a")
img <- image_data(input = uuids, size = "1024")

mammal_means$clade <- NA
mammal_means$clade[mammal_means$Baleen == "Aquatic" & mammal_means$order %in% afrotheria_orders] <- "Sirenia"
mammal_means$clade[mammal_means$Baleen == "Aquatic" & mammal_means$order %in% c("Artiodactyla","Cetacea")] <- "Odontoceti"
mammal_means$clade[mammal_means$Baleen == "Aquatic" & mammal_means$family %in% caniformia_families & !mammal_means$family == "Mustelidae"] <- "Pinnipedia"
mammal_means$clade <- factor(mammal_means$clade)

par(mar=c(5.1,4.1,3.1,4.1), yaxs="i", xaxs="i", cex = 2.25, las=1)

histStack(MeanBodyMass - 3 ~ clade, data = subset(mammal_means, Baleen == "Aquatic" & gsub(" ","_",canonicalName) %in% mammal_tree[[1]]$tip.label & !is.na(clade)), 
          breaks = seq(-2,5,.2), axes = FALSE, xlab=NA, ylab=NA, xlim=c(-2,5), ylim=c(0,30),main = "", col = c("#56B4E9","#0072B2","#CC79A7"), border = c("#56B4E9","#0072B2","#CC79A7"))

axis(side=4)
points(OUwie.parameters[c(2,4,7),"theta"] - 3, c(28.5,26.25,24), pch = 16, col = c("#CC79A7","#56B4E9","#0072B2"))
segments(OUwie.parameters[c(2,4,7),"theta"] - 3 - 1.96*(OUwie.parameters[c(2,4,7),"sd"]), c(28.5,26.25,24), OUwie.parameters[c(2,4,7),"theta"] - 3 + 1.96*(OUwie.parameters[c(2,4,7),"sd"]), c(28.5,26.25,24), lwd = 6, col = c("#CC79A7","#56B4E9","#0072B2"))
logoing_func(img[[1]], x = 3.75, y = 28.5, size = .85)
logoing_func(img[[2]], x = 3.75, y = 26.25, size = .85)
logoing_func(img[[3]], x = 3.75, y = 24, size = .85)
mtext("# of Species", side=4, line=2.5, cex = 2.25, las=0)
par(new = T)
curve(((7.5*x^.71) - (.0484*39.8*(x^.87)) - (11.4*.2*((x/3.03E-6)^(1/3.09))/1000*30/log(1/.71)))/x, .01, 100000, xlab=expression(paste("Body Mass (log"[10]," g)")), ylab="Surplus Energy (W/kg)", col = rgb(213/255, 94/255, 0/255), lwd = 6, log = "x", ylim = c(0,.6), xlim=c(.01,100000), xaxt="n")
axis(side=1, at=c(.01,.1,1,10,100,1000,10000,100000), labels = c(1:8))
legend("topleft", cex = .85, adj=c(0,1.2), y.intersp = 2.3, inset = c(.025, 0), bty= "n", c("Model\n(F - M - H)", "Modern\nDistribution", expression(paste("OUwie\nOptimum (�2",sigma,")"))), col = c(rgb(213/255, 94/255, 0/255), rgb(0/255, 114/255, 178/255), rgb(0/255, 114/255, 178/255)), lwd = 6, lty = c(1, 0, 1), pch = c(NA, 15, 16), pt.cex = c(NA, 2, 1))
#legend("bottomleft", cex = .85, inset = c(.025, 0), bty= "n", c("Sirenia", "Odontoceti", "Pinnipedia"), col = c("#56B4E9","#0072B2","#CC79A7"), pch = c(15, 15, 15), pt.cex = c(2, 2, 2))
text(x = .03, y = .04,  cex=2, adj=c(.5,.5), labels=LETTERS[2])

dev.off()

#Mammal Constraints 2####
pdf(file = "Mammal Constraints2.pdf", width = 24, height = 12)
layout(matrix(c(1,2),nrow=1, byrow = TRUE))

colors = c(rgb(230/255, 159/255, 0/255), rgb(0/255, 158/255, 115/255), rgb(86/255, 180/255, 233/255))

#individual curves (terrestrial BMR)
par(mar=c(5.1,5.1,3.1,3.1), yaxs="i", xaxs="i", cex = 2.25, las=1)
#curve(1*x^1, .01, 100000, lwd = 2, lty = "dashed", col = "grey60)
curve(7.5*x^.71, .01, 100000, lwd = 6, col = colors[1], xlab=expression(paste("Body Mass (log"[10]," kg)")), ylab=expression(paste("Rate (log"[10]," W)")), log="xy", xaxt="n", yaxt="n", ylim=c(.5,25000))
#http://www.nrcresearchpress.com/doi/pdf/10.1139/z86-047
curve(.0484*70*(x^.75), .01, 100000, lwd = 6, col = colors[2], add = TRUE)
curve(11.4*.2*((x/3.03E-6)^(1/3.09))/1000*30/log(1/.71), .01, 100000, lwd = 6, col = colors[3], add = TRUE)
axis(side=2, at=c(1,10,100,1000,10000), labels = c(0,1,2,3,4))
axis(side=1, at=c(.01,.1,1,10,100,1000,10000,100000), labels = c(-2,-1,0,1,2,3,4,5))
box()
legend("topleft", cex = .85, inset = 0, bty= "n", c("Feeding (F)","Basal Metabolism (M)","Heat Loss (H)"), col = c(colors), lwd = 6, lty = 1)

#single curve (terrestrial BMR)
par(mar=c(5.1,4.1,3.1,4.1), yaxs="i", xaxs="i", cex = 2.25, las=1)

histStack(MeanBodyMass - 3 ~ clade, data = subset(mammal_means, Baleen == "Aquatic" & gsub(" ","_",canonicalName) %in% mammal_tree[[1]]$tip.label & !is.na(clade)), 
          breaks = seq(-2,5,.2), axes = FALSE, xlab=NA, ylab=NA, xlim=c(-2,5), ylim=c(0,30),main = "", col = c("#56B4E9","#0072B2","#CC79A7"), border = c("#56B4E9","#0072B2","#CC79A7"))

axis(side=4)
points(OUwie.parameters[c(2,4,7),"theta"] - 3, c(24,26.25,28.5), pch = 16, col = c("#0072B2","#56B4E9","#CC79A7"))
segments(OUwie.parameters[c(2,4,7),"theta"] - 3 - 1.96*(OUwie.parameters[c(2,4,7),"sd"]), c(24,26.25,28.5), OUwie.parameters[c(2,4,7),"theta"] - 3 + 1.96*(OUwie.parameters[c(2,4,7),"sd"]), c(24,26.25,28.5), lwd = 6, col = c("#0072B2","#56B4E9","#CC79A7"))
logoing_func(img[[1]], x = 3.75, y = 28.5, size = .85)
logoing_func(img[[2]], x = 3.75, y = 26.25, size = .85)
logoing_func(img[[3]], x = 3.75, y = 24, size = .85)
mtext("# of Species", side=4, line=2.5, cex = 2.25, las=0)
par(new = T)
curve(((7.5*x^.71) - (.0484*70*(x^.75)) - (11.4*.2*((x/3.03E-6)^(1/3.09))/1000*30/log(1/.71)))/x, .01, 100000, xlab=expression(paste("Body Mass (log"[10]," kg)")), ylab="Surplus Energy (W/kg)", col = rgb(213/255, 94/255, 0/255), lwd = 6, log = "x", ylim = c(0,.6), xlim=c(.01,100000), xaxt="n")
axis(side=1, at=c(.01,.1,1,10,100,1000,10000,100000), labels = c(-2,-1,0,1,2,3,4,5))
legend("topleft", cex = .85, adj=c(0,1.2), y.intersp = 2.3, inset = c(.025, 0), bty= "n", c("Model\n(F - M - H)", "Modern\nDistribution", expression(paste("OUwie\nOptima (�2",sigma,")"))), col = c(rgb(213/255, 94/255, 0/255), rgb(0/255, 114/255, 178/255), rgb(0/255, 114/255, 178/255)), lwd = 6, lty = c(1, 0, 1), pch = c(NA, 15, 16), pt.cex = c(NA, 2, 1))
#legend("bottomleft", cex = .85, inset = c(.025, 0), bty= "n", c("Sirenia", "Odontoceti", "Pinnipedia"), col = c("#56B4E9","#0072B2","#CC79A7"), pch = c(15, 15, 15), pt.cex = c(2, 2, 2))

dev.off()

#Mammal Feeding####
pdf(file = "Mammal Feeding.pdf", width = 24, height = 12)
layout(matrix(c(1,2),nrow=1, byrow = TRUE))

colors = c(rgb(0,0,0), rgb(230/255, 159/255, 0/255), rgb(0/255, 158/255, 115/255), rgb(213/255,94/255,0), rgb(86/255, 180/255, 233/255))

#ingestion rates for maintenance
#https://www.jstor.org/stable/pdf/4803.pdf
par(mar=c(5.1,5.1,3.1,3.1), yaxs="i", xaxs="i", cex = 2.25, las=1)
#curve(1*x^1, .01, 100000, lwd = 2, lty = "dashed", col = "grey60")
curve(7.1*x^.72, .01, 100000, lwd = 6, col = colors[1], xlab=expression(paste("Body Mass (log"[10]," kg)")), ylab=expression(paste("Feeding Rate (log"[10]," W)")), log="xy", xaxt="n", yaxt="n", ylim=c(.5,25000))
curve(9.35*x^.74, .01, 100000, lwd = 6, lty = "dashed", col = colors[1], add = TRUE)
curve(9.68*x^.58, .01, 100000, lwd = 6, col = colors[2], add = TRUE)
curve(9.8*x^.63, .01, 100000, lwd = 6, col = colors[3], add = TRUE)
curve(10.02*x^.58, .01, 100000, lwd = 6, col = colors[4], add = TRUE)
curve(4.97*x^.89, .01, 100000, lwd = 6, col = colors[5], add = TRUE)
axis(side=2, at=c(1,10,100,1000,10000), labels = c(0,1,2,3,4))
axis(side=1, at=c(.01,.1,1,10,100,1000,10000,100000), labels = c(-2,-1,0,1,2,3,4,5))
title(main = "During Maintenance")
box()
legend("topleft", cex = .85, inset = 0, bty= "n", c("Phocidae","Terr. Carnivora","All Carnivora","Mustelidae","Terr. Carnivora-Mustelidae"), col = c(colors), lwd = 6, lty = 1)
legend("bottomright", cex = .85, inset = 0, bty= "n", c("Juveniles","Adults"), col = c("black","black"), lwd = c(6,6), lty = c("dashed","solid"))

#ingestion rates for growing
par(mar=c(5.1,5.1,3.1,3.1), yaxs="i", xaxs="i", cex = 2.25, las=1)
#curve(1*x^1, .01, 100000, lwd = 2, lty = "dashed", col = "grey60")
curve(40.2*x^.43, .01, 100000, lwd = 6, lty = "dashed", col = colors[1], xlab=expression(paste("Body Mass (log"[10]," kg)")), ylab=expression(paste("Feeding Rate (log"[10]," W)")), log="xy", xaxt="n", yaxt="n", ylim=c(.5,25000))
curve(9.64*x^.87, .01, 100000, lwd = 6, lty = "dashed", col = colors[1], add = TRUE)
curve(26.8*x^.34, .01, 100000, lwd = 6, lty = "dashed", col = colors[2], add = TRUE)
curve(6.19*x^.8, .01, 100000, lwd = 6, col = colors[3], add = TRUE)
curve(12.8*x^.51, .01, 100000, lwd = 6, lty = "dashed", col = colors[4], add = TRUE)
axis(side=2, at=c(1,10,100,1000,10000), labels = c(0,1,2,3,4))
axis(side=1, at=c(.01,.1,1,10,100,1000,10000,100000), labels = c(-2,-1,0,1,2,3,4,5))
title(main = "During Growth")
box()
legend("topleft", cex = .85, inset = 0, bty= "n", c("Phocidae","Otariidae","Pinnipeds","Terr. Carnivora"), col = c(colors), lwd = 6, lty = 1)
legend("bottomright", cex = .85, inset = 0, bty= "n", c("Juveniles","Adults"), col = c("black","black"), lwd = c(6,6), lty = c("dashed","solid"))

dev.off()


#Baleen whales####
#individual curves (phocidae BMR)
pdf(file = "Mammal Constraints-Baleen.pdf", width = 12, height = 12)
par(mar=c(5.1,4.1,3.1,4.1), yaxs="i", xaxs="i", cex = 2.25, las=1)
histStack(MeanBodyMass - 3 ~ order, data = subset(mammal_means, Baleen == "Baleen" & gsub(" ","_",canonicalName) %in% mammal_tree[[1]]$tip.label), 
          breaks = seq(-2,7,.5), axes = FALSE, xlab=NA, ylab=NA, xlim=c(-2,7), ylim=c(0,10),main = "", col = c("#56B4E9","#0072B2","#CC79A7"), border = c("#56B4E9","#0072B2","#CC79A7"))
axis(side=4)
par(new=T)
#1:1
curve(1*x^1, .01, 10000000, lwd = 2, lty = "dashed", col = "grey60", xlab=expression(paste("Body Mass (log"[10]," kg)")), ylab=expression(paste("Rate (log"[10]," W)")), log="xy", xaxt="n", yaxt="n", ylim=c(.5,2500000))
#Balaenids (from Jeremy Goldbogen)
curve(.84*3218.18*(x)^.749, .01, 10000000, lwd = 6, col = colors[1], add = TRUE)
#Rorquals (from Jeremy Goldbogen)
curve(.84*13919.06*(x)^.509, .01, 10000000, lwd = 6, col = colors[2], add = TRUE)
#http://www.nrcresearchpress.com/doi/pdf/10.1139/f88-121
#http://www.sciencedirect.com/science/article/pii/S0022519383711355
curve(11.4*.2*((x/3.03E-6)^(1/3.09))/1000*30/log(1/.71), .01, 10000000, lwd = 6, col = colors[3], add = TRUE)
#Phocidae from http://www.nrcresearchpress.com/doi/pdf/10.1139/z86-047
curve(.0484*39.8*(x^.87), .01, 10000000, lwd = 2, col = colors[4], add = TRUE)
#16.1 from http://bio.research.ucsc.edu/people/croll/pdf/Croll_2006_2.pdf
curve(12.3*x^.75, .01, 10000000, lwd = 2, col = colors[4], add = TRUE)
#16.2 from http://bio.research.ucsc.edu/people/croll/pdf/Croll_2006_2.pdf
curve(9.84*x^.756, .01, 10000000, lwd = 2, col = colors[4], add = TRUE)
#16.3 from http://bio.research.ucsc.edu/people/croll/pdf/Croll_2006_2.pdf
curve(8.88*x^.734, .01, 10000000, lwd = 2, col = colors[4], add = TRUE)
axis(side=2, at=c(1,10,100,1000,10000,100000,1000000), labels = c(0,1,2,3,4,5,6))
axis(side=1, at=c(.01,.1,1,10,100,1000,10000,100000,1000000,10000000), labels = c(-2,-1,0,1,2,3,4,5,6,7))
box()
legend("topleft", cex = .85, inset = 0, bty= "n", c("Balaenids","Rorquals","Heat Loss","Metabolism"), col = c(colors), lwd = 6, lty = 1)
mtext("# of Species", side=4, line=2.5, cex = 2.25, las=0)
dev.off()

#Baleen Theoretical Constraints####
pdf(file = "Baleen Theoretical Constraints.pdf", width = 24, height = 12)
layout(matrix(c(1,2),nrow=1, byrow = TRUE))

colors = c(rgb(230/255, 159/255, 0/255), rgb(0/255, 158/255, 115/255), rgb(86/255, 180/255, 233/255))

#individual curves (phocidae BMR)
par(mar=c(5.1,5.1,3.1,3.1), yaxs="i", xaxs="i", cex = 2.25, las=1)
#1:1
#curve(1*x^1, .01, 100000, lwd = 2, lty = "dashed", col = "grey60")
#https://www.jstor.org/stable/pdf/4803.pdf
#curve(7.5*x^.71, 1, 10000000, lwd = 6, col = colors[1], xlab=expression(paste("Body Mass (log"[10]," kg)")), ylab=expression(paste("Rate (log"[10]," W)")), log="xy", xaxt="n", yaxt="n", ylim=c(.5,25000))
#Tail includes baleens
curve(7.5*x^.78, 1, 10000000, lwd = 6, col = colors[1], xlab=expression(paste("Body Mass (log"[10]," kg)")), ylab=expression(paste("Rate (log"[10]," W)")), log="xy", xaxt="n", yaxt="n", ylim=c(.5,2500000))
#Peak at baleens
#curve(2.3*x^.87, 1, 10000000, lwd = 6, col = colors[1], xlab=expression(paste("Body Mass (log"[10]," kg)")), ylab=expression(paste("Rate (log"[10]," W)")), log="xy", xaxt="n", yaxt="n", ylim=c(.5,25000))
#http://www.nrcresearchpress.com/doi/pdf/10.1139/z86-047
curve(.0484*39.8*(x^.87), 1, 10000000, lwd = 6, col = colors[2], add = TRUE)
#http://www.nrcresearchpress.com/doi/pdf/10.1139/f88-121
#http://www.sciencedirect.com/science/article/pii/S0022519383711355
curve(11.4*.2*((x/3.03E-6)^(1/3.09))/1000*30/log(1/.71), 1, 10000000, lwd = 6, col = colors[3], add = TRUE)
axis(side=2, at=c(1,10,100,1000,10000,100000,1000000), labels = c(0,1,2,3,4,5,6))
axis(side=1, at=c(1,10,100,1000,10000,100000,1000000,10000000), labels = c(0,1,2,3,4,5,6,7))
text(x = 3, y = 1.4,  cex=2, adj=c(.5,.5), labels=LETTERS[1])
box()
legend("topleft", cex = .85, inset = 0, bty= "n", c("Feeding (F)","Basal Metabolism (M)","Heat Loss (H)"), col = c(colors), lwd = 6, lty = 1)

#single curve (phocidae BMR)
library(rphylopic)
source("histStack.R")
uuids <- c("23389deb-fe10-4c97-87c4-a606ebd1fec3","0b5c6b41-3a44-4c9e-869a-63ed54bf7c65","64c27f60-ffbf-4ee8-ae1e-726042c477ff","a67f24a9-6d75-420c-b437-6817da5cec1a")
uuids<- c(uuids, "16969246-31e0-48e3-90ff-cad9a8746073")
img <- image_data(input = uuids, size = "1024")


mammal_means$clade <- NA
mammal_means$clade[mammal_means$Baleen == "Aquatic" & mammal_means$order %in% afrotheria_orders] <- "Sirenia"
mammal_means$clade[mammal_means$Baleen == "Aquatic" & mammal_means$order %in% c("Artiodactyla","Cetacea")] <- "Odontoceti"
mammal_means$clade[mammal_means$Baleen == "Baleen" & mammal_means$order %in% c("Artiodactyla","Cetacea")] <- "Mysticeti"
mammal_means$clade[mammal_means$Baleen == "Aquatic" & mammal_means$family %in% caniformia_families & !mammal_means$family == "Mustelidae"] <- "Pinnipedia"
mammal_means$clade <- factor(mammal_means$clade)

par(mar=c(5.1,4.1,3.1,4.1), yaxs="i", xaxs="i", cex = 2.25, las=1)

histStack(MeanBodyMass - 3 ~ clade, data = subset(mammal_means, gsub(" ","_",canonicalName) %in% mammal_tree[[1]]$tip.label & !is.na(clade)), 
          breaks = seq(0,7,.2), axes = FALSE, xlab=NA, ylab=NA, xlim=c(0,7), ylim=c(0,30),main = "", col = c("#009e73","#56B4E9","#0072B2","#CC79A7"), border = c("#009e73","#56B4E9","#0072B2","#CC79A7"))

axis(side=4)
points(OUwie.parameters[c(2,4,7,5),"theta"] - 3, c(28.5,26.25,24,21.75), pch = 16, col = c("#CC79A7","#56B4E9","#0072B2","#009e73"))
segments(OUwie.parameters[c(2,4,7,5),"theta"] - 3 - 1.96*(OUwie.parameters[c(2,4,7,5),"sd"]), c(28.5,26.25,24,21.75), OUwie.parameters[c(2,4,7,5),"theta"] - 3 + 1.96*(OUwie.parameters[c(2,4,7,5),"sd"]), c(28.5,26.25,24,21.75), lwd = 6, col = c("#CC79A7","#56B4E9","#0072B2","#009e73"))
logoing_func(img[[1]], x = 3.75, y = 28.5, size = .85)
logoing_func(img[[2]], x = 3.75, y = 26.25, size = .85)
logoing_func(img[[3]], x = 3.75, y = 24, size = .85)
logoing_func(img[[5]], x = 3.25, y = 21.75, size = 1.5)
mtext("# of Species", side=4, line=2.5, cex = 2.25, las=0)
par(new = T)
#Tail includes baleens
curve(((7.5*x^.78) - (.0484*39.8*(x^.87)) - (11.4*.2*((x/3.03E-6)^(1/3.09))/1000*30/log(1/.71)))/x, 1, 10000000, xlab=expression(paste("Body Mass (log"[10]," kg)")), ylab="Surplus Energy (W/kg)", col = rgb(213/255, 94/255, 0/255), lwd = 6, log = "x", ylim = c(0,2), xlim=c(1,10000000), xaxt="n")
#Peak at baleens
#curve(((2.3*x^.87) - (.0484*39.8*(x^.87)) - (11.4*.2*((x/3.03E-6)^(1/3.09))/1000*30/log(1/.71)))/x, 1, 10000000, xlab=expression(paste("Body Mass (log"[10]," kg)")), ylab="Surplus Energy (W/kg)", col = rgb(213/255, 94/255, 0/255), lwd = 6, log = "x", ylim = c(0,.2), xlim=c(1,10000000), xaxt="n")
axis(side=1, at=c(1,10,100,1000,10000,100000,1000000,10000000), labels = c(0,1,2,3,4,5,6,7))
#legend("topleft", cex = .85, adj=c(0,1.2), y.intersp = 2.3, inset = c(.025, 0), bty= "n", c("Model\n(F - M - H)", "Modern\nDistribution", expression(paste("OUwie\nOptimum (�2",sigma,")"))), col = c(rgb(213/255, 94/255, 0/255), rgb(0/255, 114/255, 178/255), rgb(0/255, 114/255, 178/255)), lwd = 6, lty = c(1, 0, 1), pch = c(NA, 15, 16), pt.cex = c(NA, 2, 1))
#legend("bottomleft", cex = .85, inset = c(.025, 0), bty= "n", c("Sirenia", "Odontoceti", "Pinnipedia"), col = c("#56B4E9","#0072B2","#CC79A7"), pch = c(15, 15, 15), pt.cex = c(2, 2, 2))
text(x = 3, y = .13,  cex=2, adj=c(.5,.5), labels=LETTERS[2])

dev.off()