library(ape)
library(geiger)
library(paleotree)
library(phytools)
library(OUwie)

library(doSNOW)
library(foreach)

setwd("C:/Users/Will/Ozibox/STANFORD/Synapsids")

#Import Data####
synapsid_size <- read.csv("Synapsid Body Size.csv")
synapsid_size$canonicalName <- paste(synapsid_size$Genus, synapsid_size$Species)
synapsid_size$canonicalName[synapsid_size$Species == ""] <- as.character(synapsid_size$Genus[synapsid_size$Species == ""])
synapsid_size <- synapsid_size[,c(ncol(synapsid_size),3:(ncol(synapsid_size)-1))]
colnames(synapsid_size)[1] <- c("Species")

synapsid_tree <- read.nexus("Brocklehurst et al 2013 data/Brocklehurst2013_supertree.tre")
synapsid_tree_long <- read.nexus("Brocklehurst et al 2013 data/Brocklehurst_long.tre")
synapsid_tree_short <- read.nexus("Brocklehurst et al 2013 data/Brocklehurst_short.tre")
synapsid_ages <- read.csv("Brocklehurst et al 2013 data/Brocklehurst2013_ages.csv", header = FALSE)
colnames(synapsid_ages) <- c("Species", "FAD_long", "LAD_long", "FAD_short", "LAD_short")
synapsid_ages$Species <- gsub("[.]", " ", synapsid_ages$Species)
synapsid_tree$tip.label <- gsub("[.]", " ", synapsid_tree$tip.label)
rownames(synapsid_ages) <- synapsid_ages$Species
synapsid_ages <- synapsid_ages[match(synapsid_tree$tip.label, synapsid_ages$Species),2:5]
synapsid_tree_long$tip.label <- gsub("[.]", " ", synapsid_tree_long$tip.label)
synapsid_tree_short$tip.label <- gsub("[.]", " ", synapsid_tree_short$tip.label)

#PaleoTree Tree Calibration####
#Long Ages
SRres <- getSampRateCont(synapsid_ages[,1:2])
sRate <- SRres[[2]][2]
divRate <- SRres[[2]][1]
#make_durationFreqCont(synapsid_ages[,1:2])

n_trees <- 100
trees_long <- cal3TimePaleoPhy(synapsid_tree, timeData = synapsid_ages[,1:2], brRate = divRate, extRate = divRate, sampRate = sRate, ntrees = n_trees, plot = FALSE)
synDiv_long <- multiDiv(trees_long)

#Short Ages
SRres <- getSampRateCont(synapsid_ages[,3:4])
sRate <- SRres[[2]][2]
divRate <- SRres[[2]][1]
#make_durationFreqCont(synapsid_ages[,3:4])

trees_short <- cal3TimePaleoPhy(synapsid_tree, timeData = synapsid_ages[,3:4], brRate = divRate, extRate = divRate, sampRate = sRate, ntrees = n_trees, plot = FALSE)
synDiv_short <- multiDiv(trees_short)

#OUwie####
#Find mean and max sizes
require(plyr)
means <- ddply(synapsid_size, .(Species), summarise, mean = mean(SkullLength_mm, na.rm = T), sd = sd(SkullLength_mm, na.rm = T), max = max(SkullLength_mm, na.rm = T), nmeas = sum(!is.na(SkullLength_mm)))
means$se <- means$sd/sqrt(means$nmeas)

#Set final regimes
split <- 302
synapsid_ages[synapsid_ages$LAD_long > split,5]<-"1"
synapsid_ages[synapsid_ages$LAD_long < split,5]<-"2"
colnames(synapsid_ages)[ncol(synapsid_ages)] <- "Era"

#Combine ages and data
synapsid_comb <- data.frame(means[,c(1,2,4:6)],synapsid_ages[match(means$Species,rownames(synapsid_ages)),])
rownames(synapsid_comb) <- NULL
synapsid_comb <- subset(synapsid_comb, !is.na(mean))

mods <- c("BM1","BMS","OU1","OUM","OUMV","OUMA","OUMVA")

OUwieAnalysis<-function(i){
  #Make era tree (phytools)
  tree.tmp <- trees_long[[i]]
  tree.tmp$edge.length[tree.tmp$edge.length == 0] <- 1e-5
  tree.tmp <- drop.tip(tree.tmp, tree.tmp$tip.label[!(tree.tmp$tip.label %in% synapsid_comb$Species)])
  start <- max(nodeHeights(tree.tmp))
  change <- split - (tree.tmp$root.time - start)
  end <- min(nodeHeights(tree.tmp))
  tree.era <- make.era.map(tree.tmp, c(0, start - change))
  plotSimmap(tree.era)
  
  #Run OUwie analysis
  OUwieList <- list()
  for(j in 1:length(mods)){
    OUwieList[[j]] <- OUwie(tree.era, synapsid_comb[,c(1,10,2)], model = mods[j], simmap.tree = T)
  }
  return(OUwieList)
}

#Perform parallel OUwie analyses across all trees
cl<-makeCluster(4,type="SOCK")
clusterExport(cl, c("trees_long","synapsid_comb","split","mods"), envir = .GlobalEnv)
clusterCall(cl, function() library(phytools))
clusterCall(cl, function() library(OUwie))
system.time(synapsid_OUwie <- clusterApply(cl, 1:n_trees, OUwieAnalysis))
stopCluster(cl)

ouresults.AICc <- ouresults.weights <- as.data.frame(matrix(nrow = n_trees,ncol=length(mods)))
colnames(ouresults.AICc) <- colnames(ouresults.weights) <- mods
ouresults.parameters <- list()
ouresults.theta <- ouresults.alpha <- ouresults.beta <- ouresults.se <- array(NA, dim=c(2,length(mods[4:7]),n_trees), dimnames=list(c(paste(">", split, sep = ""),paste("<", split, sep = "")),mods[4:7]))

for(j in 1:n_trees){
  ouresults.parameters[[j]] <- list()
  for(i in 1:length(mods)){
    ouresults.AICc[j,i] <- synapsid_OUwie[[j]][[i]]$AICc
    if(synapsid_OUwie[[j]][[i]]$model %in% c("BMS", "OU1", "BM1")){
      ouresults.parameters[[j]][[i]] <- data.frame(cbind(synapsid_OUwie[[j]][[i]]$model,synapsid_OUwie[[j]][[i]]$AICc,t(synapsid_OUwie[[j]][[i]]$solution),synapsid_OUwie[[j]][[i]]$theta[c(1,1),]))
    }
    else{
      ouresults.parameters[[j]][[i]] <- data.frame(cbind(synapsid_OUwie[[j]][[i]]$model,synapsid_OUwie[[j]][[i]]$AICc,t(synapsid_OUwie[[j]][[i]]$solution),synapsid_OUwie[[j]][[i]]$theta))
    }
    colnames(ouresults.parameters[[j]][[i]])<-c("Model","AICc","Alpha","Sigma sq.","Optimum","SE (optimum)")
    if(synapsid_OUwie[[j]][[i]]$AICc > 0){
      if(i==4){
        ouresults.theta[,1,j] <- as.numeric(t(as.data.frame(ouresults.parameters[[j]][[i]]$Optimum)))
        ouresults.alpha[,1,j] <- as.numeric(t(as.data.frame(ouresults.parameters[[j]][[i]]$Alpha)))
        ouresults.beta[,1,j] <- as.numeric(t(as.data.frame(ouresults.parameters[[j]][[i]]$"Sigma sq.")))
        ouresults.se[,1,j] <- as.numeric(t(as.data.frame(ouresults.parameters[[j]][[i]]$"SE (optimum)")))
      }
      else if(i==5){
        ouresults.theta[,2,j] <- as.numeric(t(as.data.frame(ouresults.parameters[[j]][[i]]$Optimum)))
        ouresults.alpha[,2,j] <- as.numeric(t(as.data.frame(ouresults.parameters[[j]][[i]]$Alpha)))
        ouresults.beta[,2,j] <- as.numeric(t(as.data.frame(ouresults.parameters[[j]][[i]]$"Sigma sq.")))
        ouresults.se[,2,j] <- as.numeric(t(as.data.frame(ouresults.parameters[[j]][[i]]$"SE (optimum)")))
      }
      else if(i==6){
        ouresults.theta[,3,j] <- as.numeric(t(as.data.frame(ouresults.parameters[[j]][[i]]$Optimum)))
        ouresults.alpha[,3,j] <- as.numeric(t(as.data.frame(ouresults.parameters[[j]][[i]]$Alpha)))
        ouresults.beta[,3,j] <- as.numeric(t(as.data.frame(ouresults.parameters[[j]][[i]]$"Sigma sq.")))
        ouresults.se[,3,j] <- as.numeric(t(as.data.frame(ouresults.parameters[[j]][[i]]$"SE (optimum)")))
      }
      else if(i==7){
        ouresults.theta[,4,j] <- as.numeric(t(as.data.frame(ouresults.parameters[[j]][[i]]$Optimum)))
        ouresults.alpha[,4,j] <- as.numeric(t(as.data.frame(ouresults.parameters[[j]][[i]]$Alpha)))
        ouresults.beta[,4,j] <- as.numeric(t(as.data.frame(ouresults.parameters[[j]][[i]]$"Sigma sq.")))
        ouresults.se[,4,j] <- as.numeric(t(as.data.frame(ouresults.parameters[[j]][[i]]$"SE (optimum)")))
      }
    }
  }
  if(!(is.na(ouresults.AICc[j,6]) || ouresults.AICc[j,6] < 0)){
    weights <- exp(-.5*(ouresults.AICc[j,]-min(ouresults.AICc[j,])))
    ouresults.weights[j,] <- weights/sum(weights)
  }
  else{
    weights <- exp(-.5*(ouresults.AICc[j,c(1:5,7)]-min(ouresults.AICc[j,c(1:5,7)])))
    ouresults.weights[j,c(1:5,7)] <- weights/sum(weights)
  }
}

ouresults.AICc.summary <- array(NA, dim=c(3,7), dimnames=list(c("Minimum","Median","Maximum"),mods))
ouresults.AICc.summary["Minimum",] <- apply(ouresults.weights, 2, min, na.rm = T)
ouresults.AICc.summary["Median",] <- apply(ouresults.weights, 2, median, na.rm = T)
ouresults.AICc.summary["Maximum",] <- apply(ouresults.weights, 2, max, na.rm = T)
ouresults.theta.summary <- array(NA, dim=c(2,4,3), dimnames=list(c(paste(">", split, sep = ""),paste("<", split, sep = "")),mods[4:7],c("Minimum","Median","Maximum")))
ouresults.theta.summary[,,"Minimum"] <- apply(ouresults.theta, c(1,2), min, na.rm = T)
ouresults.theta.summary[,,"Median"] <- apply(ouresults.theta, c(1,2), median, na.rm = T)
ouresults.theta.summary[,,"Maximum"] <- apply(ouresults.theta, c(1,2), max, na.rm = T)
ouresults.beta.summary <- array(NA, dim=c(2,4,3), dimnames=list(c(paste(">", split, sep = ""),paste("<", split, sep = "")),mods[4:7],c("Minimum","Median","Maximum")))
ouresults.beta.summary[,,"Minimum"] <- apply(ouresults.beta, c(1,2), min, na.rm = T)
ouresults.beta.summary[,,"Median"] <- apply(ouresults.beta, c(1,2), median, na.rm = T)
ouresults.beta.summary[,,"Maximum"] <- apply(ouresults.beta, c(1,2), max, na.rm = T)
ouresults.alpha.summary <- array(NA, dim=c(2,4,3), dimnames=list(c(paste(">", split, sep = ""),paste("<", split, sep = "")),mods[4:7],c("Minimum","Median","Maximum")))
ouresults.alpha.summary[,,"Minimum"] <- apply(ouresults.alpha, c(1,2), min, na.rm = T)
ouresults.alpha.summary[,,"Median"] <- apply(ouresults.alpha, c(1,2), median, na.rm = T)
ouresults.alpha.summary[,,"Maximum"] <- apply(ouresults.alpha, c(1,2), max, na.rm = T)
ouresults.se.summary <- array(NA, dim=c(2,4,3), dimnames=list(c(paste(">", split, sep = ""),paste("<", split, sep = "")),mods[4:7],c("Minimum","Median","Maximum")))
ouresults.se.summary[,,"Minimum"] <- apply(ouresults.se, c(1,2), min, na.rm = T)
ouresults.se.summary[,,"Median"] <- apply(ouresults.se, c(1,2), median, na.rm = T)
ouresults.se.summary[,,"Maximum"] <- apply(ouresults.se, c(1,2), max, na.rm = T)

boxplot(subset(ouresults.weights, BM1>1e-25))
title("Model Support for Synapsid Body Size Evolution",ylab="AICc Weight")
ouresults.AICc.summary
cbind(ouresults.alpha.summary[,1,],ouresults.beta.summary[,1,],ouresults.theta.summary[,1,],ouresults.se.summary[,1,])
cbind(ouresults.alpha.summary[,2,],ouresults.beta.summary[,2,],ouresults.theta.summary[,2,],ouresults.se.summary[,2,])
cbind(ouresults.alpha.summary[,3,],ouresults.beta.summary[,3,],ouresults.theta.summary[,3,],ouresults.se.summary[,3,])
cbind(ouresults.alpha.summary[,4,],ouresults.beta.summary[,4,],ouresults.theta.summary[,4,],ouresults.se.summary[,4,])

theta.plot <- as.data.frame(matrix(nrow = 2, ncol = 3))
colnames(theta.plot) <- c("Period","theta","se")
theta.plot[,1] <- c("Before Herbivory","After Herbivory")
theta.plot[,1] <- factor(theta.plot[,1], levels=c("Before Herbivory","After Herbivory"))
theta.plot[1,2] <- ouresults.theta.summary[1,2,2]
theta.plot[1,3] <- ouresults.se.summary[1,2,2]
theta.plot[2,2] <- ouresults.theta.summary[2,2,2]
theta.plot[2,3] <- ouresults.se.summary[2,2,2]

require(ggplot2)
ggplot(theta.plot,aes(x=Period,y=theta)) +
  geom_errorbar(aes(ymax = theta + 2*se, ymin=theta - 2*se), color = "blue", width=.2) +
  geom_point() +
  xlab("Period") +
  ylab("Optimum (mm)") +
  ggtitle("Optima as Determined by the OUMV Model")

#Plotting Sizes As Stratigraphic Ranges####
windows()
par(las=1, mar=c(0,4.5,0,0.7), pch=16)
layout(matrix(c(1,2), nrow=2, ncol=1, byrow=TRUE), widths=1, heights=c(1/10, 9/10))
plot(1:100, axes=FALSE, type="n", xlab="",ylab="", xlim=c(0,100), ylim=c(0,2), frame.plot=FALSE)
x <- 25
points(x, 1, cex=2.3, col="black")
text(x+3, 1, labels="Prior to 302 Ma",  cex=1.25, adj=0)
x <- 60
points(x, 1, cex=2.3, col="red")
text(x+3, 1, labels="After 302 Ma",  cex=1.25, adj=0)
par(mar=c(4.3, 4.5, 0, 0.7), yaxs="i", xaxs="i", cex=1.5, cex.axis=1.5, cex.lab=1.5)
plot(1:10, type="n", xlab=expression("Geologic time (Ma)"), ylab=expression(paste("Skull Length (mm)")), xlim=c(330, 250), ylim=c(0,400))

segments(x0 = synapsid_comb$FAD_short, y0 = synapsid_comb$mean, x1 = synapsid_comb$LAD_short, y1 = synapsid_comb$mean, lwd=2)
segments(x0 = 302, y0 = 0, x1 = 302, y1 = 400, lty = "dashed", lwd = 2, col = "blue")
rect(xleft = 330, ybottom = (theta.plot$theta[1] - 2*theta.plot$se[1]), xright = 250, ytop = (theta.plot$theta[1] + 2*theta.plot$se[1]), density = 3, col = adjustcolor("black",alpha.f = .40), border = NA, lwd = 2)
rect(xleft = 330, ybottom = (theta.plot$theta[2] - 2*theta.plot$se[2]), xright = 250, ytop = (theta.plot$theta[2] + 2*theta.plot$se[2]), density = 3, col = adjustcolor("red",alpha.f = .40), border = NA, lwd = 2)



